package com.example.rayshshen.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.rayshshen.cache.LruBitmapCache;
import com.example.rayshshen.data.Constant;
import com.example.rayshshen.data.Park;
import com.example.rayshshen.myapplication.CustomListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Shu-Hua Shen on 2016/4/2.
 */
public class HttpConnector {

    public static final String TAG = "HttpConnector";

    private static HttpConnector mInstance;
    private RequestQueue mRequestQueue = null;
    private ImageLoader mImageLoader;
    private static Context mContext;

    private HttpConnector(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized HttpConnector getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new HttpConnector(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag){
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        if(mImageLoader == null) {
            mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
        }
        return mImageLoader;
    }

    public void cancelPendingRequest(Object tag) {
        if(mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void fetchData(String url, final List<Park> parks, final CustomListAdapter customListAdapter) {
        StringRequest req = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.v(TAG, "response :" + response);
                    JSONObject obj = new JSONObject(response);
                    JSONObject result = obj.getJSONObject(Constant.KEY_RESULT);
                    JSONArray arrayObj = result.getJSONArray(Constant.KEY_RESULTS);
                    for (int i = 0; i < arrayObj.length(); i++) {
                        Park park = new Park();
                        JSONObject results = arrayObj.getJSONObject(i);
                        park.setName(results.getString(Park.KEY_PARK_NAME));
                        park.setIntroduction(results.getString(Park.KEY_INTRODUCTION));
                        park.setImageLink(results.getString(Park.KEY_IMAGE_LINK));
                        parks.add(park);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                customListAdapter.notifyDataSetChanged();
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "error  : " + error.networkResponse);
            }
        });
        HttpConnector.getInstance(mContext).addToRequestQueue(req, TAG);
    }

    public boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                Toast.makeText(context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                Toast.makeText(context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
                return true;
            }
        }
        return false;
    }

}