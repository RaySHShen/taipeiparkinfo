package com.example.rayshshen.myapplication;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.example.rayshshen.data.Constant;
import com.example.rayshshen.data.Park;
import com.example.rayshshen.network.HttpConnector;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private CustomListAdapter mListAdapter;
    private ListView mListView;

    private List<Park> mParks;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        if(!HttpConnector.getInstance(mContext).isNetworkConnected(mContext)) {
            Toast.makeText(mContext,R.string.tips_open_network, Toast.LENGTH_LONG).show();
            finish();
        }
        mListView = (ListView) findViewById(R.id.list);
        mParks = new ArrayList<Park>();
        mListAdapter = new CustomListAdapter(mContext, mParks);
        HttpConnector.getInstance(mContext).fetchData(Constant.TAIPEI_SERVER_API + Constant.RID_TAIPEI_PARK, mParks, mListAdapter);
        mListView.setAdapter(mListAdapter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        HttpConnector.getInstance(mContext).cancelPendingRequest(HttpConnector.TAG);
    }

}
