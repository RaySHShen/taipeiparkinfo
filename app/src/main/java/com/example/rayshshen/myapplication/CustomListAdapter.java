package com.example.rayshshen.myapplication;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.rayshshen.data.Park;
import com.example.rayshshen.network.HttpConnector;

import java.util.List;

/**
 * Created by Shu-Hua Shen on 2016/4/2.
 */
public class CustomListAdapter extends BaseAdapter {

    private LayoutInflater mInflater = null;
    private List<Park> mParks;
    private Context mContext;
    private ImageLoader mImageLoader;

    public CustomListAdapter(Context context, List<Park> parks) {
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mParks = parks;
        mImageLoader = HttpConnector.getInstance(mContext.getApplicationContext()).getImageLoader();
    }

    private static class ViewHolder {
        NetworkImageView networkImageView;
        TextView parkName, introduction;
    }

    @Override
    public int getCount() {
        return mParks.size();
    }

    @Override
    public Object getItem(int position) {
        return mParks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        if(convertView == null) {
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.custom_list_view, null);
            holder.networkImageView = (NetworkImageView) convertView.findViewById(R.id.park_image);
            holder.networkImageView.setDefaultImageResId(R.drawable.loading);
            holder.networkImageView.setErrorImageResId(R.drawable.error);
            holder.parkName = (TextView) convertView.findViewById(R.id.txt_park_name);
            holder.introduction = (TextView) convertView.findViewById(R.id.txt_introduction);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.networkImageView.setImageUrl(mParks.get(position).getImageLink(), mImageLoader);
        holder.parkName.setText(mParks.get(position).getParkName());
        holder.introduction.setText(mParks.get(position).getIntroduction());
        return convertView;
    }
}