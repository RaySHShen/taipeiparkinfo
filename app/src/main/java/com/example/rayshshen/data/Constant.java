package com.example.rayshshen.data;

/**
 * Created by Shu-Hua Shen on 2016/4/2.
 */
public class Constant {
    public final static String TAIPEI_SERVER_API = "http://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=";
    public final static String RID_TAIPEI_PARK = "bf073841-c734-49bf-a97f-3757a6013812";

    public final static String KEY_RESULT = "result";
    public final static String KEY_RESULTS = "results";
}
