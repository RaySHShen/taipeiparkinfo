package com.example.rayshshen.data;

/**
 * Created by Shu-Hua Shen on 2016/4/2.
 */
public class Park {
    public final static String KEY_PARK_NAME = "Name";
    public final static String KEY_IMAGE_LINK = "Image";
    public final static String KEY_INTRODUCTION = "Introduction";

    private String mParkName;
    private String mImageLink;
    private String mIntroduction;

    public void setName(String name) {
        this.mParkName = name;
    }

    public String getParkName() {
        return mParkName;
    }

    public void setImageLink(String link) {
        this.mImageLink = link;
    }

    public String getImageLink() {
        return mImageLink;
    }

    public void setIntroduction(String introduction) {
        this.mIntroduction = introduction;
    }

    public String getIntroduction() {
        return mIntroduction;
    }

}
